/**
 * 作業01 - 矩陣相乘 (使用 Strassen's Algorithm)
 */
public class MatrixProduct {

    /**
     * 回傳 A * B，使用 Strassen's Algorithm
     *
     * @param A 矩陣 (m * n)
     * @param B 矩陣 (n * k)
     * @return C 為 A * B 的結果 (m * k)
     */
    public int[][] solve(int[][] A, int[][] B) {
        // TODO: 請實作作業
        return new int[][]{};
    }

}
